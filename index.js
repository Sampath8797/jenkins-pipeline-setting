'use strict';
const express = require('express');
// Constants
const PORT = 8000;
const HOST = '0.0.0.0';
// App
const app = express();
let date_ob = new Date();
let date = ("0" + date_ob.getDate()).slice(-2);
let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
let year = date_ob.getFullYear();
let hours = date_ob.getHours();
let minutes = date_ob.getMinutes();
let seconds = date_ob.getSeconds();
app.get('/', (req, res) => {
  res.send("Date"  + year +  "-"  + month +  "-"  + date +  " Time "  + hours +  ":"  + minutes +  ":"  + seconds);
  res.send("It trigered on the this time");

});
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);